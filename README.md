# HexMapper - server

HexMapper is a websocket-based application for multiplayer hex map interactions. It was made to allow multiple users to edit hex tiles by marking and clearing them with multiple, player-based colors in real time. Created as, but not limited to, board games testing, planning and structurizing tool.

This repository is the server part of the system. Built with `Node v18.15.0`, `socket.io`, `TypeScript`, `express`, `morgan` and more.

For the client part of the application, see [HexMapper - client](https://gitlab.com/jkthomas/hexmapper-client) repository.

## Development

To install all of the required packages, run:

```bash
yarn
```

If any of the prerequisites are missing, terminal messages will point you to a sources for download and installation. You can now run the server or develop the code!

To launch the server locally, just do:

```bash
yarn run dev
```

A successful command should result in the logs as below:

![alt text](static/yarn_dev_result.png)

As it can be seen from the screenshot above, the default server URL is `http://localhost:4000`. You can access the server there.

## State

The current state of the `HexMapper` is **stable, in development**. There are many TODOs including both issues and features, but the overall system is working and can be manually tested locally and when deployed.

There are minimal logs enabled by default, currently only logging the connection statuses of users:
![alt text](static/example_minimal_logs.png)

Example client view:

![alt text](static/example_client_view.png)

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

## License

[MIT](https://choosealicense.com/licenses/mit/)
