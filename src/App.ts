import express, { Express } from 'express';
import cookieParser from 'cookie-parser';
import logger from 'morgan';
import { createServer, Server } from 'http';
import { IndexRouter } from './routes/IndexRouter';
import dotenv from 'dotenv';
import { SocketServer } from './Socket/SocketServer';

dotenv.config();
const App: Express = express();
const server: Server = createServer(App);

App.use(logger('dev'));
App.use(express.json());
App.use(express.urlencoded({ extended: false }));
App.use(cookieParser());

IndexRouter.registerRoutes(App);

const socketServer = new SocketServer(server);
socketServer.setUp();

server.listen(process.env.PORT || 4000, () => {
  console.log(`Server listening on port ${process.env.PORT || 4000}.`);
});

export default App;
