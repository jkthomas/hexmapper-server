interface BoardHex {
  coordQ: number;
  currentStatus: string;
  originalStatus: string;
}

interface BoardRow {
  coordR: number;
  hexes: Array<BoardHex>;
}

export interface EmitBoardOutput {
  sizeQ: number;
  sizeR: number;
  rows: Array<BoardRow>;
}
