export interface EmitAcceptUsernameOutput {
  username: string;
}
