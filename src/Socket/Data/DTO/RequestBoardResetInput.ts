export interface RequestBoardResetInput {
  boardSizeQ?: number;
  boardSizeR?: number;
  voids?: number;
  rocks?: number;
}
