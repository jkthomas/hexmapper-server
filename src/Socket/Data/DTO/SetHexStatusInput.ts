export interface SetHexStatusInput {
  coordQ: number;
  coordR: number;
  markingOption: string;
}
