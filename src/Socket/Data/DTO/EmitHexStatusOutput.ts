export interface EmitHexStatusOutput {
  newStatus: string;
  coordR: number;
  coordQ: number;
}
