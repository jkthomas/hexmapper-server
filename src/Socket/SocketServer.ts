import { Server, Socket } from 'socket.io';
import { Server as HttpServer } from 'http';
import { ClientService } from '../Client/ClientService';
import { MarkingOption } from '../Game/Data/Static/MarkingOption';
import { GameService } from '../Game/GameService';
import { HexStatus } from '../Game/Data/Static/HexStatus';
import { SetHexStatusInput } from './Data/DTO/SetHexStatusInput';
import { EmitPlayersOutput } from './Data/DTO/EmitPlayersOutput';
import { EmitHexStatusOutput } from './Data/DTO/EmitHexStatusOutput';
import { SocketEvents } from './Data/Static/SocketEvents';
import { EmitBoardOutput } from './Data/DTO/EmitBoardOutput';
import { SetUsernameInput } from './Data/DTO/SetUsernameInput';
import { RequestBoardResetInput } from './Data/DTO/RequestBoardResetInput';
import { EmitAcceptUsernameOutput } from './Data/DTO/EmitAcceptUsernameOutput';

export class SocketServer {
  private io: Server;
  private gameService: GameService;
  private clientService: ClientService;

  constructor(httpServer: HttpServer) {
    this.io = new Server(httpServer, {
      path: '/game',
      pingInterval: 10000,
      pingTimeout: 5000,
      cors: {
        origin: 'http://localhost:3000',
        credentials: true,
      },
      transports: ['websocket'],
    });
    console.log(`Socket IO initialized on port ${process.env.PORT || 4000}`);

    this.gameService = new GameService(11, 15, 4, 5);
    this.clientService = new ClientService();
  }

  setUp(): void {
    //TODO: Remove coupling between Client and Game services (Player)
    //TODO: Add object <-> output/input mapper
    //TODO: Add logging for each start/end of operation
    //TODO: Switch to Socket Namespace (supports middleware)
    this.io.on(SocketEvents.Client.Connect, (socket: Socket) => {
      console.log(`Client ID connected: ${socket.id}`);
      //TODO: Add reconnect
      //TODO: Add username validation middleware
      //TODO: Add check-alive event for both back/front ends
      socket.on(SocketEvents.Game.SetUsername, (input: SetUsernameInput) => {
        console.log(`Client with ID: ${socket.id} set username to: ${input.username}`);
        const isNewClientConnectHandled: boolean = this.clientService.handleNewClientConnect(socket.id, input.username);
        if (!isNewClientConnectHandled) {
          // socket.disconnect();
          socket.emit(SocketEvents.General.Error, 'Bad name provided or max players (4) reached.');
          return;
        }
        //TODO: Split incoming and outcoming events - CQRS pattern
        const emitAcceptUsernameOutput: EmitAcceptUsernameOutput = { username: input.username };
        socket.emit(SocketEvents.Game.EmitAcceptUsername, emitAcceptUsernameOutput);
        const emitPlayersOutput: EmitPlayersOutput[] = this.clientService.getClients().map((client) => ({
          name: client.clientName,
          color: client.clientColorMain,
        }));
        this.io.sockets.emit(SocketEvents.Game.EmitPlayers, emitPlayersOutput);
      });

      // socket.on(SocketEvents.General.Error, (error: any) => {
      //   console.log(`Error occured: ${error}`);
      // });

      socket.on(SocketEvents.Client.Disconnect, () => {
        console.log(`Disconnected client ID: ${socket.id}`);
        this.clientService.handleClientDisconnection(socket.id);
        const emitPlayersOutput: EmitPlayersOutput[] = this.clientService.getClients().map((client) => ({
          name: client.clientName,
          color: client.clientColorMain,
        }));
        this.io.sockets.emit(SocketEvents.Game.EmitPlayers, emitPlayersOutput);
      });

      socket.on(SocketEvents.Game.RequestPlayers, () => {
        const emitPlayersOutput: EmitPlayersOutput[] = this.clientService.getClients().map((client) => ({
          name: client.clientName,
          color: client.clientColorMain,
        }));
        this.io.sockets.emit(SocketEvents.Game.EmitPlayers, emitPlayersOutput);
      });

      socket.on(SocketEvents.Game.RequestBoard, () => {
        if (socket.id) {
          const board = this.gameService.getBoard();
          const emitBoardOutput: EmitBoardOutput = {
            sizeQ: board.sizeQ,
            sizeR: board.sizeR,
            rows: board.rows.map((row) => {
              return {
                coordR: row.coordR,
                hexes: row.hexes.map((hex) => {
                  return {
                    coordQ: hex.coordQ,
                    currentStatus: hex.currentStatus,
                    originalStatus: hex.originalStatus,
                  };
                }),
              };
            }),
          };
          socket.emit(SocketEvents.Game.EmitBoard, emitBoardOutput);
        }
      });

      socket.on(SocketEvents.Game.SetHexStatus, (input: SetHexStatusInput) => {
        let newStatus: HexStatus | undefined = this.gameService.getHexOriginalStatus(input.coordQ, input.coordR);
        //TODO: Add status verify - no overrides of same status
        try {
          if (input.markingOption === MarkingOption.Player) {
            newStatus = this.clientService.getClientColors(socket.id).main as HexStatus;
          } else if (input.markingOption === MarkingOption.Alt) {
            newStatus = this.clientService.getClientColors(socket.id).alt as HexStatus;
          }
        } catch (e: any) {
          console.log(e.message);
        }

        if (newStatus) {
          this.gameService.updateHexStatus(input.coordQ, input.coordR, newStatus);
          const emitHexStatusOutput: EmitHexStatusOutput = {
            newStatus: newStatus,
            coordR: input.coordR,
            coordQ: input.coordQ,
          };
          this.io.sockets.emit(SocketEvents.Game.EmitHexStatus, emitHexStatusOutput);
        }
      });

      socket.on(SocketEvents.Game.RequestBoardReset, (input: RequestBoardResetInput) => {
        this.gameService.resetBoard(input.boardSizeQ, input.boardSizeR, input.voids, input.rocks);
        const board = this.gameService.getBoard();
        const emitBoardOutput: EmitBoardOutput = {
          sizeQ: board.sizeQ,
          sizeR: board.sizeR,
          rows: board.rows.map((row) => {
            return {
              coordR: row.coordR,
              hexes: row.hexes.map((hex) => {
                return {
                  coordQ: hex.coordQ,
                  currentStatus: hex.currentStatus,
                  originalStatus: hex.originalStatus,
                };
              }),
            };
          }),
        };
        this.io.sockets.emit(SocketEvents.Game.EmitBoard, emitBoardOutput);
      });
    });
  }
}
