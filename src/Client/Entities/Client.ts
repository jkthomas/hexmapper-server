import { ClientColorType } from '../Data/Types/ClientColorType';

export class Client {
  clientID: string;
  clientName: string;
  clientColorMain: string;
  clientColorAlt: string;

  constructor(clientID: string, clientName: string, clientColor: ClientColorType) {
    this.clientID = clientID;
    this.clientName = clientName;
    this.clientColorMain = clientColor.main;
    this.clientColorAlt = clientColor.alt;
  }
}
