import { Client } from './Entities/Client';
import { ClientColorType } from './Data/Types/ClientColorType';
import { ClientColor } from './Data/Static/ClientColor';

export class ClientService {
  private clients: Client[] = [];

  private getAvailableColor(): ClientColorType | undefined {
    const takenColors: string[] = this.clients.map((client) => client.clientColorMain);
    let playerColor: ClientColorType | undefined = undefined;
    ClientColor.Colors.forEach((color: ClientColorType) => {
      if (!takenColors.includes(color.main)) {
        playerColor = color;
      }
    });
    return playerColor;
  }

  private canAddNewClient(name: string): boolean {
    const takenNames: string[] = this.clients.map((client) => client.clientName);
    return this.clients.length < 4 && name !== '' && !takenNames.includes(name);
  }

  getClients(): Client[] {
    return this.clients;
  }

  getClientColors(id: string): ClientColorType {
    const client = this.clients.find((client) => client.clientID === id);
    if (client) {
      return { main: client.clientColorMain, alt: client.clientColorAlt };
    }
    return { main: '', alt: '' };
  }

  handleNewClientConnect(id: string, name: string): boolean {
    let isSuccessful: boolean = false;
    if (this.canAddNewClient(name)) {
      const playerColor = this.getAvailableColor();
      if (playerColor) {
        this.clients.push(new Client(id, name, playerColor));
        isSuccessful = true;
      }
    }
    return isSuccessful;
  }

  handleClientDisconnection(id: string): boolean {
    let isSuccessful: boolean = false;
    const disconnectedClientIndex: number = this.clients.findIndex((client) => client.clientID === id);
    if (disconnectedClientIndex >= 0) {
      this.clients.splice(disconnectedClientIndex, 1);
      isSuccessful = true;
    }
    return isSuccessful;
  }
}
