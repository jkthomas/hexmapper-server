export type ClientColorType = {
  main: string;
  alt: string;
};
