import { ClientColorType } from '../Types/ClientColorType';

export class ClientColor {
  public static readonly Red: string = 'red';
  public static readonly RedAlt: string = 'pink';
  public static readonly Green: string = 'green';
  public static readonly GreenAlt: string = 'lightgreen';
  public static readonly Blue: string = 'blue';
  public static readonly BlueAlt: string = 'lightblue';
  public static readonly Brown: string = 'brown';
  public static readonly BrownAlt: string = 'lightsalmon';

  public static readonly Colors: ClientColorType[] = [
    { main: ClientColor.Red, alt: ClientColor.RedAlt },
    { main: ClientColor.Green, alt: ClientColor.GreenAlt },
    { main: ClientColor.Blue, alt: ClientColor.BlueAlt },
    { main: ClientColor.Brown, alt: ClientColor.BrownAlt },
  ];
}
