export enum HexStatus {
  Empty = 'empty',
  Void = 'void',
  Rock = 'rock',
  Red = 'red',
  RedAlt = 'pink',
  Green = 'green',
  GreenAlt = 'lightgreen',
  Blue = 'blue',
  BlueAlt = 'lightblue',
  Brown = 'brown',
  BrownAlt = 'lightsalmon',
}
