export enum MarkingOption {
  Player = 'player',
  Alt = 'alt',
  Erase = 'erase',
}
