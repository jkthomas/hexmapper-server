import { Board } from './Entities/Board';
import { BoardGenerator } from './Generation/BoardGenerator';
import { HexStatus } from './Data/Static/HexStatus';
import { ObstaclesGenerator } from './Generation/ObstaclesGenerator';

export class GameService {
  private board: Board;

  constructor(boardSizeQ?: number, boardSizeR?: number, voids?: number, rocks?: number) {
    this.board = BoardGenerator.generateEmptyBoard(boardSizeQ || 11, boardSizeR || 15);
    this.initializeBoardObstacles(voids, rocks);
  }

  private initializeBoardObstacles(voids?: number, rocks?: number): void {
    ObstaclesGenerator.generateObstacles(this.board, voids || 4, HexStatus.Void);
    ObstaclesGenerator.generateObstacles(this.board, rocks || 5, HexStatus.Rock);
  }

  getBoard(): Board {
    return this.board;
  }

  getHexOriginalStatus(coordQ: number, coordR: number): HexStatus | undefined {
    const originalStatus = this.board.rows
      .find((row) => row.coordR === coordR)
      ?.hexes.find((hex) => hex.coordQ === coordQ)?.originalStatus;
    return originalStatus;
  }

  updateHexStatus(coordQ: number, coordR: number, status: HexStatus): boolean {
    let isSuccessful: boolean = false;
    const hex = this.board.rows.find((row) => row.coordR === coordR)?.hexes.find((hex) => hex.coordQ === coordQ);
    if (hex && status) {
      hex.currentStatus = status;
      isSuccessful = true;
    }
    return isSuccessful;
  }

  resetBoard(boardSizeQ?: number, boardSizeR?: number, voids?: number, rocks?: number): boolean {
    let isSuccessful: boolean = false;
    try {
      this.board = BoardGenerator.generateEmptyBoard(boardSizeQ || 11, boardSizeR || 15);
      this.initializeBoardObstacles(voids, rocks);
      isSuccessful = true;
    } catch (e: any) {
      console.log(e.message);
    }
    return isSuccessful;
  }
}
