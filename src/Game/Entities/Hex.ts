export class Hex {
  coordQ: number;
  coordR: number;
  isOccupied = false;

  constructor(coordQ: number, coordR: number) {
    this.coordQ = coordQ;
    this.coordR = coordR;
  }
}
