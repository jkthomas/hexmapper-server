import { HexStatus } from '../Data/Static/HexStatus';

export class BoardHex {
  coordQ: number;
  currentStatus: HexStatus;
  originalStatus: HexStatus;

  constructor(coordQ: number, originalStatus = HexStatus.Empty) {
    this.coordQ = coordQ;
    this.originalStatus = originalStatus;
    this.currentStatus = originalStatus;
  }
}
