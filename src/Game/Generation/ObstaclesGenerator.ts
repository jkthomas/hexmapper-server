import { Board } from '../Entities/Board';
import { BoardHex } from '../Entities/BoardHex';
import { HexStatus } from '../Data/Static/HexStatus';

export namespace ObstaclesGenerator {
  function setHexStatus(hex: BoardHex | undefined, hexStatus: HexStatus): void {
    if (hex) {
      hex.currentStatus = hexStatus;
      hex.originalStatus = hexStatus;
    }
  }
  export function generateObstacles(board: Board, maxObstaclesNumber: number, hexStatus: HexStatus): void {
    /**
     * When generating obstacles, algorithm pick one Hex from:
     * 1) -X to 0 of coordR
     * 2) Any possible value of coorQ in chosen row
     * Then count the rest (3) hexes containing obstacles
     * 3) (-q,-r)
     * 4) (q, -(q+r))[pt.1/2] (-q, q+r)[pt.3] <-- from first picked Hex or opposite Hex coords value (pt.1/2 or pt.3)
     * 5) (q, -(q+r)) <-- from second picked Hex (pt.3)
     *  */
    const maxObstacleGenerationR = board.rows.findIndex((row) => row.coordR === 0);

    for (let obstaclesNumber = 0; obstaclesNumber < maxObstaclesNumber; obstaclesNumber++) {
      // 1
      const row = board.rows.slice(0, maxObstacleGenerationR + 1)[
        Math.floor(Math.random() * (maxObstacleGenerationR + 1))
      ];
      // 2
      let hex: BoardHex | undefined = row.hexes[Math.floor(Math.random() * row.hexes.length)];
      if (hex.currentStatus !== HexStatus.Empty) {
        //Subtract obstacles count that will be added upon loop continuing
        obstaclesNumber -= 1;
        continue;
      }
      const hexQ = hex.coordQ;
      const hexR = row.coordR;
      setHexStatus(hex, hexStatus);
      // 3
      hex = board.rows.find((row) => row.coordR === -hexR)?.hexes.find((hex) => hex.coordQ === -hexQ);
      setHexStatus(hex, hexStatus);
      // 4
      hex = board.rows.find((row) => row.coordR === -hexR)?.hexes.find((hex) => hex.coordQ === hexQ + hexR);
      setHexStatus(hex, hexStatus);
      // 5
      hex = board.rows.find((row) => row.coordR === hexR)?.hexes.find((hex) => hex.coordQ === -(hexQ + hexR));
      setHexStatus(hex, hexStatus);
    }
  }
}
