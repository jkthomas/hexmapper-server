import { Board } from '../Entities/Board';
import { BoardRow } from '../Entities/BoardRow';
import { BoardHex } from '../Entities/BoardHex';

export namespace BoardGenerator {
  function generateEmptyBoardRowHexes(minQ: number, maxQ: number): Array<BoardHex> {
    const coordsQ = Array.from({ length: maxQ - minQ + 1 }, (_, i) => i + minQ);
    const boardRowHexes: Array<BoardHex> = Array.from(coordsQ, (coordQ) => {
      return new BoardHex(coordQ);
    });
    return boardRowHexes;
  }

  function generateEmptyBoardRow(sizeR: number, coordR: number): BoardRow {
    let minQ = 0;
    let maxQ = 0;
    const deviationCoordR = (sizeR - 1) / 2;
    minQ = -deviationCoordR - Math.floor(coordR / 2);
    maxQ = deviationCoordR - Math.ceil(coordR / 2);

    return new BoardRow(coordR, generateEmptyBoardRowHexes(minQ, maxQ));
  }

  export function generateEmptyBoard(sizeQ: number, sizeR: number): Board {
    const minR = -(sizeQ - 1) / 2;
    const maxR = (sizeQ - 1) / 2;
    const rows: Array<BoardRow> = [];
    for (let coordR = minR; coordR <= maxR; coordR++) {
      rows.push(generateEmptyBoardRow(sizeR, coordR));
    }

    return new Board(sizeQ, sizeR, rows);
  }
}
