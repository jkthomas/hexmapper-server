import { Express, Request, Response } from 'express';

export namespace IndexRouter {
  export function registerRoutes(App: Express): void {
    App.get('/', (req: Request, res: Response) => {
      res.status(201).send('data');
      // res.status(400).send({ error: 'error message' });
    });
  }
}
